package com.example.myapplication;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Turkey extends AppCompatActivity {
    String API_KEY = "e2b15072fb34449283fb056ff442f670"; // ### YOUE NEWS API HERE ###
    String NEWS_SOURCE = "abc-news"; // Other news source code at: https://newsapi.org/sources
    ListView listaNovosti;
    ProgressBar loader;
    ProgressDialog progressdialog;
    TextView data1;
    String Api3,Api4,Api5;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    static final String KEY_AUTOR = "author";
    static final String KEY_NASLOV = "title";
    static final String KEY_DETALJI = "description";
    static final String KEY_URL = "url";
    static final String KEY_URLTOIMAGE = "urlToImage";
    static final String KEY_VREME = "publishedAt";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Bundle bundle = getIntent().getExtras();
        Api3= bundle.getString("Api3");
        getSupportActionBar().setTitle("Turkey News");
        listaNovosti = findViewById(R.id.listaNovosti);
        loader = findViewById(R.id.loader);
        listaNovosti.setEmptyView(loader);


        if (Function.isNetworkAvailable(getApplicationContext())) {
            DownloadNews newsTask = new DownloadNews();
            newsTask.execute();
        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(this)



                    .setTitle("Alert")

                    .setMessage("Please check your internet connection")

                    .setCancelable(false)
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            finish();
                        }
                    })



                    .show();
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
        }

    }


    @SuppressLint("StaticFieldLeak")
    public class DownloadNews extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        protected String doInBackground(String... args) {
/*
            String xml = Function.excuteGet("https://newsapi.org/v2/top-headlines?country=rs" + NEWS_SOURCE + "&sortBy=top&apiKey=" + API_KEY);
*/
            // String xml = Function.excuteGet("https://newsapi.org/v2/top-headlines?country=rs" );
            String xml = Function.excuteGet(Api3);
            return xml;
        }

        @Override
        protected void onPostExecute(String xml) {

            if (xml.length() > 10) {

                try {
                    JSONObject jsonResponse = new JSONObject(xml);
                    JSONArray jsonArray = jsonResponse.optJSONArray("articles");

                    assert jsonArray != null;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        HashMap<String, String> map = new HashMap<>();
                        map.put(KEY_AUTOR, jsonObject.optString(KEY_AUTOR));
                        map.put(KEY_NASLOV, jsonObject.optString(KEY_NASLOV));
                        map.put(KEY_DETALJI, jsonObject.optString(KEY_DETALJI));
                        map.put(KEY_URL, jsonObject.optString(KEY_URL));
                        map.put(KEY_URLTOIMAGE, jsonObject.optString(KEY_URLTOIMAGE));
                        map.put(KEY_VREME, jsonObject.optString(KEY_VREME));

                        dataList.add(map);
                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "Unexpected error", Toast.LENGTH_SHORT).show();
                }

                final ListaNovostiAdapter adapter = new ListaNovostiAdapter(Turkey.this, dataList);
                listaNovosti.setAdapter(adapter);



                listaNovosti.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view,
                                            final int position, long id) {


                        Intent i = new Intent(Turkey.this, DetaljiActivity.class);
                        i.putExtra("url", dataList.get(+position).get(KEY_URL));
                        startActivity(i);

                    }
                });


            } else {
                Toast.makeText(getApplicationContext(), "No news found", Toast.LENGTH_SHORT).show();
            }

        }

    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(),Country_list.class);
        startActivity(intent);

    }
}
