package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class Country_list  extends AppCompatActivity {

    Button button1,button2,button3,button4,button5;
    String Api1="https://newsapi.org/v2/top-headlines?country=rs",Key1="e2b15072fb34449283fb056ff442f670";//serbia
    String Api2="https://newsapi.org/v2/top-headlines?country=fr",Key2="e2b15072fb34449283fb056ff442f670";//france
    String Api3="https://newsapi.org/v2/top-headlines?country=tr",Key3="e2b15072fb34449283fb056ff442f670";//turkey
    String Api4="https://newsapi.org/v2/top-headlines?country=in",Key4="e2b15072fb34449283fb056ff442f670";//india
    String Api5="https://newsapi.org/v2/top-headlines?country=us",Key5="e2b15072fb34449283fb056ff442f670";//America
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_country);
        inliz();
    }

    public void inliz(){
        button1=findViewById(R.id.Srbija);
        button2=findViewById(R.id.Francuska);
        button3=findViewById(R.id.Turska);
        button4=findViewById(R.id.Indija);
        button5=findViewById(R.id.Amerika);
button1.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Country_list.this,MainActivity.class);
        Bundle b = new Bundle();
        b.putString("Api1",Api1);

        intent.putExtras(b);
        startActivity(intent);
    }
});

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Country_list.this,France.class);
                Bundle b = new Bundle();
                b.putString("Api2",Api2);

                intent.putExtras(b);
                startActivity(intent);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Country_list.this,Turkey.class);
                Bundle b = new Bundle();
                b.putString("Api3",Api3);

                intent.putExtras(b);
                startActivity(intent);
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Country_list.this,India.class);
                Bundle b = new Bundle();
                b.putString("Api4",Api4);

                intent.putExtras(b);
                startActivity(intent);
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Country_list.this,America.class);
                Bundle b = new Bundle();
                b.putString("Api5",Api5);

                intent.putExtras(b);
                startActivity(intent);
            }
        });




    }
    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }
}
