package com.example.myapplication;

import android.widget.BaseAdapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

class ListaNovostiAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<HashMap<String, String>> data;

    public ListaNovostiAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
    }
    public int getCount() {
        return data.size();
    }
    public Object getItem(int position) {
        return position;
    }
    public long getItemId(int position) {
        return position;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        ListaNovostiViewHolder holder = null;
        if (convertView == null) {
            holder = new ListaNovostiViewHolder();
            convertView = LayoutInflater.from(activity).inflate(
                    R.layout.lista_novosti, parent, false);
            holder.slika = (ImageView) convertView.findViewById(R.id.slika);
            holder.autor = (TextView) convertView.findViewById(R.id.autor);
            holder.naslov = (TextView) convertView.findViewById(R.id.naslov);
            holder.detalji = (TextView) convertView.findViewById(R.id.detalji);
            holder.vreme = (TextView) convertView.findViewById(R.id.vreme);
            convertView.setTag(holder);
        } else {
            holder = (ListaNovostiViewHolder) convertView.getTag();
        }
        holder.slika.setId(position);
        holder.autor.setId(position);
        holder.naslov.setId(position);
        holder.detalji.setId(position);
        holder.vreme.setId(position);

        HashMap<String, String> song = new HashMap<String, String>();
        song = data.get(position);

        try{
            holder.autor.setText(song.get(MainActivity.KEY_AUTOR));
            holder.naslov.setText(song.get(MainActivity.KEY_NASLOV));
            holder.vreme.setText(song.get(MainActivity.KEY_VREME));
            holder.detalji.setText(song.get(MainActivity.KEY_DETALJI));

            if(song.get(MainActivity.KEY_URLTOIMAGE).toString().length() < 5)
            {
                holder.slika.setVisibility(View.GONE);
            }else{
                Picasso.get()
                        .load(song.get(MainActivity.KEY_URLTOIMAGE))
                        .resize(300, 200)
                        .centerCrop()
                        .into(holder.slika);
            }
        }catch(Exception e) {}
        return convertView;
    }
}

class ListaNovostiViewHolder {
    ImageView slika;
    TextView autor, naslov, detalji, vreme;
}